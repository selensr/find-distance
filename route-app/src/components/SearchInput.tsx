import React, { FunctionComponent, useEffect, Fragment } from "react";
import { CircularProgress, FormControl, TextField } from "@mui/material";
import Autocomplete from "@mui/material/Autocomplete";
import { getCities } from "../server/api";
import { City } from "../typeDefs";

const SearchInput: FunctionComponent<{
  value?: City;
  setValue?: (val: City) => void;
}> = ({ value, setValue }) => {
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [inputValue, setInputValue] = React.useState("");
  const loading = options.length === 0 && inputValue.length > 0;

  useEffect(() => {
    if (inputValue) {
      (async () => {
        const resp = await getCities(inputValue);
        if (resp) {
          setOptions(resp);
        }
      })();
    }
  }, [loading, inputValue]);

  useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  return (
    <FormControl
      variant="standard"
      sx={{
        textAlign: "center",
        alignItems: "center",
        justifyContent: "center",
        p: "1rem",
        width: "100%",
      }}
    >
      <Autocomplete
        open={open}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
        }}
        isOptionEqualToValue={(option, value) => option.city === value.city}
        getOptionLabel={(option) => option.city}
        options={options}
        loading={loading}
        renderInput={(params) => (
          <TextField
            {...params}
            label="City"
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <Fragment>
                  {loading ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                  {params.InputProps.endAdornment}
                </Fragment>
              ),
            }}
          />
        )}
        sx={{ width: "100%" }}
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
        inputValue={inputValue}
        onInputChange={(event, newValue) => {
          setInputValue(newValue);
        }}
      />
    </FormControl>
  );
};

export default SearchInput;
