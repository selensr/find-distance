import React, { useReducer } from "react";
import style from "./style.module.scss";
import { SearchInput } from "../components";
import { Button, TextField, Typography } from "@mui/material";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { City } from "../typeDefs";
import { encodeQueryData } from "../util/helper";
import * as yup from "yup";

interface State {
  cities: City[];
}

const Home = () => {
  const initialState: State = { cities: [] };

  function reducer(
    state: State,
    action: { type: string; payload?: City; currentCity?: City }
  ) {
    switch (action.type) {
      case "addCity":
        return { cities: [...state.cities, action.payload] };

      case "updateCity":
        const firstSlice = state.cities.slice(
          0,
          state.cities.indexOf(action.currentCity)
        );
        const secondSlice = [
          action.payload,
          ...state.cities.slice(state.cities.indexOf(action.currentCity) + 1),
        ];
        const newCities = [...firstSlice, ...secondSlice];

        return { cities: newCities };

      case "removeCity":
        return {
          cities: state.cities.filter((city) => city !== action.payload),
        };
      default:
        throw new Error();
    }
  }
  const [state, dispatch] = useReducer(reducer, initialState);

  const [originCity, setOriginCity] = React.useState<City | null>(null);
  const [destination, setDestination] = React.useState<City | null>(null);
  const [dateValue, setDateValue] = React.useState<Date | null>(null);
  const [passengers, setPassengers] = React.useState<number>(0);

  const handleCalculateRoute = () => {
    const route = {
      originCity: originCity?.city,
      destination: destination?.city,
      dateValue: new Date(dateValue).getTime(),
      passengers,
      intermediateCities: state.cities.map((city) => city.city),
    };

    const schema = yup.object().shape({
      originCity: yup.string().required(),
      destination: yup.string().required(),
      dateValue: yup.number().required(),
      passengers: yup.number().required(),
      intermediateCities: yup.array().of(yup.string()),
    });

    schema.isValid(route).then((valid) => {
      if (valid) {
        const query = encodeQueryData(route);
        window.location.href = `/search?${query}`;
      } else {
        alert("Please fill all the required fields");
      }
    });
  };

  return (
    <div className={style.page}>
      <Typography variant="h2" sx={{ p: "2rem" }}>
        Plan Your Trip
      </Typography>
      <div className={style.container}>
        <div className={style.tripInfo}>
          <DesktopDatePicker
            label={"Choose your date*"}
            inputFormat="MM/dd/yyyy"
            value={dateValue}
            onChange={(value: Date) => setDateValue(value)}
            renderInput={(params) => <TextField {...params} />}
          />
          <TextField
            required
            label="Passengers"
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
            value={passengers}
            onChange={(e: React.BaseSyntheticEvent) =>
              setPassengers(e.target.value)
            }
          />
        </div>

        <div className={style.mainSearchGroup}>
          <div>
            <Typography variant="h5">Choose city for pickup* </Typography>
            <SearchInput value={originCity} setValue={setOriginCity} />
          </div>
          <div>
            <Typography variant="h5">Add Direction*</Typography>
            <SearchInput value={destination} setValue={setDestination} />
          </div>
        </div>

        {state.cities.length > 0 &&
          state.cities.map((cityObj) => {
            return (
              <div key={cityObj.city} className={style.stationSearchGroup}>
                <Typography variant="h5">Add Stop</Typography>
                <div style={{ display: "flex", width: "100%" }}>
                  <SearchInput
                    value={cityObj}
                    setValue={(val) => {
                      if (val) {
                        dispatch({
                          type: "updateCity",
                          currentCity: cityObj,
                          payload: val,
                        });
                      }
                    }}
                  />

                  <Button
                    onClick={() =>
                      dispatch({ type: "removeCity", payload: cityObj })
                    }
                    variant="contained"
                    sx={{ m: "1rem" }}
                  >
                    Remove
                  </Button>
                </div>
              </div>
            );
          })}

        <div className={style.stationSearchGroup}>
          <Typography variant="h5">Add Stop</Typography>
          <SearchInput
            value={{
              city: "",
              coordinateX: 0,
              coordinateY: 0,
            }}
            setValue={(val) => {
              if (val) {
                dispatch({
                  type: "addCity",
                  payload: val,
                });
              }
            }}
          />
        </div>

        <Button
          variant="contained"
          sx={{ mt: "1rem" }}
          onClick={() => handleCalculateRoute()}
        >
          Show Route
        </Button>
      </div>
    </div>
  );
};

export default Home;
