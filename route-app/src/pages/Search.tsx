import React, { useEffect } from "react";
import style from "./style.module.scss";
import { CircularProgress, TextField, Typography } from "@mui/material";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { getCity } from "../server/api";
import { City } from "../typeDefs";
import { calculateDistance } from "../util/helper";

const Search = () => {
  const [cities, setCities] = React.useState([]);
  const [distances, setDistances] = React.useState([]);
  const [totalRouteDistance, setTotalRouteDistance] = React.useState(0);

  const [originCity, setOriginCity] = React.useState<City | null>(null);
  const [destination, setDestination] = React.useState<City | null>(null);
  const [dateValue, setDateValue] = React.useState<Date | null>(null);
  const [passengers, setPassengers] = React.useState<number>(0);
  const [intermediateCities, setintermediateCities] = React.useState([]);

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const dateValueParam = urlParams.get("dateValue");
    const passengersParam = urlParams.get("passengers");
    const origin = urlParams.get("originCity");
    const destination = urlParams.get("destination");
    const intermediateCitiesParam = urlParams.get("intermediateCities");
    setDateValue(new Date(parseInt(dateValueParam)));
    setPassengers(parseInt(passengersParam));

    (async () => {
      const resp = await getCity(origin);
      setOriginCity(resp);
      const respDestination = await getCity(destination);
      setDestination(respDestination);

      const intermediateCitiesArr = intermediateCitiesParam
        ? intermediateCitiesParam.split(",")
        : [];

      if (intermediateCitiesArr.length > 0) {
        Promise.all(
          intermediateCitiesParam.split(",").map(async (city) => {
            const response = await getCity(city);
            return response[0];
          })
        ).then((res) => setintermediateCities(res));
      }
    })();
  }, []);

  useEffect(() => {
    if (originCity && destination && intermediateCities.length > 0) {
      setCities([originCity[0], ...intermediateCities, destination[0]]);
    } else if (originCity && destination) {
      setCities([originCity[0], destination[0]]);
    }
  }, [intermediateCities, originCity, destination]);

  useEffect(() => {
    if (cities.length > 0) {
      const distances = cities.map((city, index) => {
        const origin = index === 0 ? originCity[0] : cities[index - 1];
        const destination = city;
        const distance = calculateDistance(
          origin.coordinateX,
          origin.coordinateY,
          destination.coordinateX,
          destination.coordinateY
        );
        return Math.floor(distance);
      });

      setDistances(distances.filter((distance) => distance !== 0));
      setTotalRouteDistance(distances.reduce((a, b) => a + b, 0));
    }
  }, [cities, originCity]);

  return (
    <div className={style.page}>
      <Typography variant="h2" sx={{ p: "2rem" }}>
        Review Your Trip
      </Typography>
      <div className={style.container}>
        <div className={style.tripInfo}>
          <DesktopDatePicker
            label={"Choosen date"}
            inputFormat="MM/dd/yyyy"
            value={dateValue}
            onChange={() => {}}
            renderInput={(params) => <TextField {...params} />}
            disabled
          />
          <TextField
            label="Passengers"
            type="number"
            value={passengers}
            disabled
          />
        </div>

        <div className={style.mainSearchGroup}>
          <div>
            <Typography variant="h5">Origin City</Typography>
            <TextField
              type="text"
              value={originCity?.[0]?.city}
              disabled
              sx={{
                width: "100%",
                p: "1rem",
              }}
            />
          </div>
          <div>
            <Typography variant="h5">Destination</Typography>
            <TextField
              type="text"
              value={destination?.[0]?.city}
              disabled
              sx={{
                width: "100%",
                p: "1rem",
              }}
            />
          </div>
        </div>

        {intermediateCities.length > 0 &&
          intermediateCities.map((cityObj, index) => {
            return (
              <div key={cityObj.city} className={style.stationSearchGroup}>
                <Typography variant="h5">Stop {index + 1}</Typography>
                <TextField
                  type="text"
                  value={cityObj.city}
                  disabled
                  sx={{
                    width: "100%",
                    p: "1rem",
                  }}
                />
              </div>
            );
          })}

        <Typography variant="h5" sx={{ p: "0.5rem" }}>
          Distances Between Cities:
        </Typography>

        {cities.length > 0 && distances.length > 0 ? (
          <div className={style.cities}>
            <Typography variant="body1">{cities[0].city}</Typography>
            {cities.slice(1).map((cityObj, index) => {
              return (
                <>
                  <Typography variant="body1">
                    <br />
                    {distances[index]}km
                  </Typography>
                  <Typography variant="body1">{cityObj.city}</Typography>
                </>
              );
            })}
          </div>
        ) : (
          <CircularProgress />
        )}

        <Typography variant="h4" sx={{ p: "0.5rem" }}>
          Total Route Distance: {totalRouteDistance}km
        </Typography>
      </div>
    </div>
  );
};

export default Search;
