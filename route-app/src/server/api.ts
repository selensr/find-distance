const baseUrl = "https://find-distance-backend-app.herokuapp.com";

export async function getCities(keyword) {
  const response = await fetch(`${baseUrl}/cities?city_like=${keyword}`);
  if (!response.ok) {
    throw { status: 500, message: "Failed to fetch searched cities" };
  }
  return response.json();
}

export async function getCity(keyword) {
  const response = await fetch(`${baseUrl}/cities?city=${keyword}`);
  if (!response.ok) {
    throw { status: 500, message: "Failed to fetch city" };
  }
  return response.json();
}
