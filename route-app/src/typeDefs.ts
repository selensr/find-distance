export type City = {
  city: string;
  coordinateX: number;
  coordinateY: number;
};
